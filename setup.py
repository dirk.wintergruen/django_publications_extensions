from setuptools import setup, find_packages

setup(
    name='django_publications_extensions',
    version='0.20b3',
    #packages=['publications_extensions', 'publications_extensions.management',
    #          'publications_extensions.management.commands', 'publications_extensions.migrations',
    #          'publications_extensions.helpers','publications_extensions.helpers.process_pdf',
    #          'publications_extensions.templates'],
    packages=find_packages(),
    url='',
    license='',
    author='dwinter',
    author_email='',
    description='',
    #packages=find_packages(),
	include_package_data=True,
)
