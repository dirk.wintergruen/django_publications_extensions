import logging
import os,os.path

import requests
from django.conf import settings
from django.core.management import BaseCommand
import os
import subprocess
import uuid

from django.core.files import File

from documents.models import Source, Page
from publications.models.attachment import Attachment, AttachmentType, PDFAttachment
from filer.models import File as Filer_File, Folder
#GROBID_URL = "http://dw3.mpiwg-berlin.mpg.de:8070/api/"
#TEMP_PATH= "/tmp/pdf2django"
import logging

from publications_extensions.helpers.filesHandler import uploadOrGetExistingFile
from publications_extensions.models import PublicationSourceReference

logger = logging.getLogger(__name__)


#UNITE = "/usr/bin/pdfunite"
#UNITE = "/usr/local/anaconda3/bin/pdfunite"
def unite(p_s_r):

    os.makedirs("/tmp/unite",exist_ok=True)
    outf = "/tmp/unite/" + str(uuid.uuid4()) + ".pdf"

    if not isinstance(p_s_r,PublicationSourceReference):

        p_s_r = PublicationSourceReference.objects.get(pk=int(p_s_r))

    pages = p_s_r.source.getOrderedListOfPages()



    files = []
    for p in pages:
        page = Page.objects.get(pk=p)
        if os.path.exists(page.path):
            files.append(page.path)
        else:
            logging.warning(f"Page {p} path_path {page.path} does not exist.")



    args = [settings.UNITE + " " + " ".join(files + [outf])]
    result = subprocess.run(args,shell=True)#, capture_output= True)

    new_obj = uploadOrGetExistingFile(outf,
                            f"{p_s_r.source.pk}.pdf",
                            "PDF",
                            always_upload = False)

    typ,created = AttachmentType.objects.get_or_create(name="pdf_from_pages")
    pdf_attm = Attachment(name=f"{p_s_r.source.pk}.pdf", type_of_text = typ)

    pdf_attm.file = new_obj
    pdf_attm.parent = p_s_r.publication
    pdf_attm.save()

    return [f"Created {pdf_attm}"],[pdf_attm]







