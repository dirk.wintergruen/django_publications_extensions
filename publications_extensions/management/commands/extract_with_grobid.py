import logging
import os,os.path

import requests
from django.core.management import BaseCommand

from publications.models.collection import Collection


class Command(BaseCommand):
    help = 'extracts pdfs in a collection using grobid'

    def processFile(self,file_path,
                    out_file_path,
                    grobit_url, errorout=None):

                with open(file_path, "rb") as inf:
                    pdf = inf.read()
                res = requests.post(grobit_url + "processFulltextDocument", files={"input": pdf})
                if not res.status_code == 200:
                    logging.error("Error: %s" % file_path)
                    if errorout is not None:
                        errorout.write("%s\n" % file_path)
                else:
                    with open(out_file_path, "wb") as outf:
                        outf.write(res.content)
                    logging.info("ok: %s" % out_file_path)

    def findCollection(self,cid=None,
                       cname=None,
                       ckey=None):


        if ckey is not None:
            logging.info(f"ckey ({ckey} is set, I will use this to find the collection")
            collection = Collection.objects.get(zoterokey=ckey)
        else:
            collection = Collection.objects.get(pk=cid,
                                            name = cname)

        return collection



    def extract_collection(self,collection,tei_path,grobid_url,
                           ignore_existing=False):
        existing = 0
        for item,paths in  collection.get_pdfs().items():
            for p in paths:
                path,fn = os.path.split(p)
                name,ext = os.path.splitext(fn)
                out = os.path.join(tei_path, name + ".xml")
                out_lower =  os.path.join(tei_path, name.lower() + ".xml")
                out_upper = os.path.join(tei_path, name.upper() + ".xml")
                if not ignore_existing:
                    if os.path.exists(out) or os.path.exists(out_lower) or os.path.exists(out_upper):
                        existing += 1
                        logging.info("ex: %s" % out)
                        continue
                self.processFile(p,out,grobid_url)


    def add_arguments(self, parser):
        parser.add_argument('--collection_id', type=int,
                            default = None,
                            help="django id of the collection")
        parser.add_argument('--collection_name', type=str,
                            default = None,
                            help="name of the collection, if not unique use the id or zotero_key")
        parser.add_argument("--collection_zoterokey", type=str,
                            default=None,
                            help="zotero_key of the collection")

        parser.add_argument("--tei_path",type=str,
                            required = True,
                            help="path where tei files are stored")
        parser.add_argument("--grobid_url",type=str,
                                default="http://localhost:8070/api/")

    def handle(self, *args, **options):
        logging.basicConfig(level=logging.DEBUG)

        try:
            collection = self.findCollection(cid=options["collection_id"],
                                         cname =options["collection_name"],
                                         ckey =options["collection_zoterokey"]
                                         )
        except Collection.MultipleObjectsReturned:
            print("Collection is not unique!")
            return
        except Collection.DoesNotExist:
            print("Collection does not exists!")
            return

        self.extract_collection(collection=collection,
                     tei_path= options["tei_path"],
                     grobid_url= options["grobid_url"])


