import os,os.path
import sys
from collections import defaultdict

from django.core.management import BaseCommand
from django.core.files import File
from publications.models import Collection
from publications.models.attachment import URLAttachment, PDFAttachment, Attachment, AttachmentType
from publications.models.tag import Tag
import logging

from publications_extensions.helpers import filesHandler

logger = logging.getLogger(__name__)
from filer.models import File as Filer_File, Folder


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--in_folder', type=str, required = True)
        parser.add_argument('--collection', type=str, required = True, help= "name of the collection")
        parser.add_argument('--out_folder', type=str, required=False, help="Folder name in django filer")
        parser.add_argument("--tags", type=str,default="", help="comma separated list of tags")
        parser.add_argument("--type", type=str, required= True, help="type of the attachment")
        parser.add_argument("--upload", const=True, default=False, nargs="?", required=False, help="upload the file")
        parser.add_argument("--always_upload", default=False, const=True, nargs="?",
                            help="always upload the file, normally checks if file with same sha already exists.")
        #parser.add_argument("--always_upload", default=False, const=True, nargs="?",
        #                    help="always upload the file, normally checks if file with same sha already exists.")

    def handle(self, *args, **options):
        #logging.basicConfig(level=logging.DEBUG)

        type = options["type"]

        try:
            type = AttachmentType.objects.get(name=type)
        except AttachmentType.DoesNotExist:
            types = [x.name for x in AttachmentType.objects.all()]
            print(f"Type {type} is not a registered type please create it first in django!")
            print(f"I only now: {types}")
            return

        in_folder = options["in_folder"]


        tags = []
        for tag in options["tags"].split(","):
            new_tag,created = Tag.objects.get_or_create(name=tag)
            tags.append(new_tag)
            new_tag.save()

        collection = Collection.objects.get(name=options["collection"])
        filename2item = defaultdict(list)
        for item in collection.items.all():
            pdfattachement = item.pdfattachment_set.all()
            for pdf in pdfattachement:
                if pdf.file:
                    name,ext = os.path.splitext(pdf.file.original_filename)
                    filename2item[name.lower()].append(item)

        if not os.path.exists(in_folder):
            print(f"{in_folder} does not exist")
            sys.exit(-1)

        for root,dirs,files in os.walk(in_folder):
            for f in files:
                url = "file://" + os.path.join(root,f)
                #parent_key,ext = os.path.splitext(f)

                parent_key = f

                #two possibilities --- filename is key of a publication object or of an PDF attachement try first publications:
                #publications = Publication.objects.filter(**{fn_field:parent_key})
                name, ext = os.path.splitext(f)
                publications =filename2item.get(name.lower(), None)
                if not publications:
                    print (f"cannot find parent for {name}")
                    continue
                else:
                    print(f"Found parent for {name}")
                for publication in publications:

                    # create an urlattachment
                    objs_tmp = URLAttachment.objects.filter(url=url)
                    objs = [o for o in objs_tmp if o.parent == publication]
                    if len(objs) == 0:
                        urlattachment = URLAttachment(parent=publication,url=url)
                    elif len(objs)> 1:
                        raise ValueError
                    else:
                        urlattachment = objs[0]
                    urlattachment.save()
                    for t in tags:
                        urlattachment.tags.add(t)
                    urlattachment.type_of_text = type
                    urlattachment.save()

                    if not options["upload"] and not options["always_upload"]:
                        continue

                    folder_name = options["out_folder"]
                    if folder_name is None:
                        print("if you select upload or always upload you have to set --out_folder")
                    path = os.path.join(root,f)

                    new_obj = filesHandler.uploadOrGetExistingFile(path,
                            f,
                            folder_name,
                            options["always_upload"])

                    logger.debug(f"will create attachment now")

                    attachment, created = Attachment.objects.get_or_create(parent=publication,name=f,type_of_text=type)
                    if created:
                        logger.debug("created new pdf attachment!")
                    attachment.file = new_obj
                    logger.debug(f"adding tags")
                    for t in tags:
                        attachment.tags.add(t)
                    # attachment.parent = Publication.objects.get_or_create(zoterokey=parentItem)
                    new_obj.save()
                    attachment.save()
                    attachment.related_URLAttachment.add(urlattachment)
                    #attachment.type = type
                    attachment.save()