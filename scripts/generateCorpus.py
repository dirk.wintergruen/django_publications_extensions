"""generate a corpus from the pdfs
pdfs are splitted by 'headings' a heading is indentified as text block with a style attribute and the
average content of this block is smaller (and larger) than a given threshold.
"""

import os,os.path
from collections import defaultdict

from textacy import Corpus
from tqdm import tqdm

from documents.models import Page
from publications.models import Collection
from publications_extensions.models import PublicationSourceReference

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
import pandas as pd
import spacy
import textacy
import textacy.vsm
import textacy.tm
import matplotlib.pyplot as plt
import pickle
import lxml
from publications.models.attachment import *

def getStyle2Text(pages):
    from collections import defaultdict
    style2text = defaultdict(list)
    for page in pages:
        for ps in page.xpath("./alto:PrintSpace",namespaces=ns):
            for block in ps:
                if not "STYLEREFS" in block.attrib:
                    continue
                #print(block.attrib["STYLEREFS"])
                #print(block.attrib["ID"])
                btext = ""
                for line in block:
                    #print(line)
                    #print(line.attrib)
                    for x in line:
                        if x.tag == "{http://www.loc.gov/standards/alto/ns-v3#}SP":
                            #print(" ")
                            btext += " "
                        else:
                            #print(x.attrib)
                            #print(x.attrib["CONTENT"])
                            btext += x.attrib.get("CONTENT",f"::{x.tag}::")
                style2text[block.attrib["STYLEREFS"]].append(btext)
    return style2text

def getCandsForHeading(style2text,min_threshold,max_threshold):
    cands = []
    for k,v in style2text.items():
        length = len(v)
        length_all = sum([len(x) for x in v])
        av_len = length_all / length
        if av_len < max_threshold and av_len > min_threshold:
            #print(k)
            cands.append(k)
    print(len(cands))
    return cands

def getChunks(pages,cands):
    chunks= {}
    from collections import defaultdict
    style2text = defaultdict(list)
    chunk= []
    ident = None
    for page in pages:
        for ps in page.xpath("./alto:PrintSpace",namespaces=ns):
            for block in ps:
                if not "STYLEREFS" in block.attrib:
                    continue
                #print(block.attrib["STYLEREFS"])
                #print(block.attrib["ID"])
                if ident is None:
                    ident = block.attrib["ID"]

                if block.attrib["STYLEREFS"] in cands: #possible heading
                    chunks[ident] = chunk
                    chunk = []
                    ident = block.attrib["ID"]

                btext = ""
                for line in block:
                    #print(line)
                    #print(line.attrib)
                    for x in line:
                        if x.tag == "{http://www.loc.gov/standards/alto/ns-v3#}SP":
                            #print(" ")
                            btext += " "
                        else:
                            #print(x.attrib)
                            #print(x.attrib["CONTENT"])
                           # btext += x.attrib.get("CONTENT",f"::{x.tag}::")
                         btext += x.attrib.get("CONTENT",f"::XX::")
                style2text[block.attrib["STYLEREFS"]].append(btext)
                chunk.append(btext)
    chunks[ident] = chunk ## lastone
    return chunks

atts = AttachmentType.objects.all()
tei_type = AttachmentType.objects.get(name="alto")
altos = Attachment.objects.filter(type_of_text=tei_type)

ns = {"alto": "http://www.loc.gov/standards/alto/ns-v3#"}

txt2chunks = {}
for alto in altos:
    print(alto.name)
    xml = lxml.etree.parse(alto.file.path)
    pages = xml.xpath("//alto:Page",namespaces=ns)
    style2text = getStyle2Text(pages)
    cands = getCandsForHeading(style2text,20,100)
    chunks = getChunks(pages,cands)
    txt2chunks[alto.name] = chunks

col = Collection.objects.get(pk=2)
src2alto = defaultdict(list)
for pub in col.items.all():
    try:
        p_s_r = PublicationSourceReference.objects.get(publication=pub)
    except:
        continue
    source = p_s_r.source
    print(source.pk)
    pages = source.getOrderedListOfPages()
    for p in pages:
        p = Page.objects.get(pk=p)
        head, tail = os.path.split(p.path)
        fn, ext = os.path.splitext(tail)

        pages = Page.objects.filter(path__contains=fn, type="alto")

        for p2 in pages:
            source2 = p2.getSources()
            if source in source2:
                # print(p_s_r.publication.title)
                src2alto[p_s_r.publication].append(p2)

import copy

new_xml = {}
for k, altos in src2alto.items():
    txt = []

    xmlMain = None
    for alto in altos:
        xml = lxml.etree.parse(alto.path)
        if xmlMain is None:
            xmlMain = xml
        else:
            root = xml.getroot()
            for r in root:
                # new_element = lxml.etree.fromstring(lxml.etree.tostring(r))
                # print(new_element)
                new_element = copy.deepcopy(r)
                xmlMain.getroot().append(new_element)

    new_xml[k] = xmlMain

txt2chunks2 = {}
records = []
for name,xml in new_xml.items():
    print(name)
    #xml = lxml.etree.parse(alto.file.path)
    pages = xml.xpath("//alto:Page",namespaces=ns)
    style2text = getStyle2Text(pages)
    cands = getCandsForHeading(style2text,20,100)
    chunks = getChunks(pages,cands)
    txt2chunks2[name] = chunks

for obj, chunks in txt2chunks.items():
    for k,chunk in chunks.items():
        md = { "title":obj,
             "chunkid": k}

        records.append((" ".join(chunk),md))

corpus = Corpus(lang="de_core_news_lg")

for r in tqdm(records):
    corpus.add_record(r)

records = []
for obj, chunks in txt2chunks2.items():
    for k,chunk in chunks.items():
        md = { "title":obj,
             "chunkid": k}

        records.append((" ".join(chunk),md))

for r in tqdm(records):
    corpus.add_record(r)

import pickle
with open("/tmp/corpus.pickle","wb") as outf:
    pickle.dump(corpus,outf)