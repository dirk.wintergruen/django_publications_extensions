import os
import subprocess
import uuid

from django.core.files import File
from publications.models.attachment import Attachment, AttachmentType, PDFAttachment
from filer.models import File as Filer_File, Folder
from django.conf import settings

import logging
logger = logging.getLogger(__name__)

def transform(in_f,  mode="txt"):
    """

    :param in_f: django pdfattachment or pk of django pdfattachment should contain a pdf at file object für input
    :param mode:
    :return:
    """
    if isinstance(in_f,int):
        in_f = PDFAttachment.objects.get(pk=in_f)

    in_f_path = in_f.file.path

    os.makedirs(settings.TEMP_PATH,exist_ok=True)

    out_f = os.path.join(settings.TEMP_PATH,uuid.uuid4().urn)

    if mode == "xml":
        out_f = out_f + ".xml"
        cmd = f"{settings.PDFTOHTML} -hidden -xml -s {in_f_path} {out_f}"
    elif mode == "txt":
        out_f = out_f + ".txt"
        cmd = f"{settings.PDFTOTEXT} {in_f_path} {out_f}"
    else:
        raise ValueError(f"Mode  {mode} unknown!")

    returned_value = subprocess.run(cmd.split(" "),stderr=subprocess.STDOUT)
    if returned_value.returncode != 0:
        logger.error(f"Couldn't create text/html from {in_f_path}")
        logger.error(returned_value.stdout)
        logger.error(returned_value)
        logger.error(cmd.split(" "))
        return None
    else:
        logger.debug(returned_value.stdout)
        if mode == "txt":
            type = AttachmentType.objects.get(name="pdf_text")
            suffix = ".txt"
            folder, created = Folder.objects.get_or_create(name="pdf_text")
        elif mode =="xml":
            type,created = AttachmentType.objects.get_or_create(name="xml")
            suffix = ".xml"
            folder,created = Folder.objects.get_or_create(name="xml")
        else:
            raise ValueError (f"Mode  {mode} unknown!")

        name = in_f.file.original_filename
        attachment, created = Attachment.objects.get_or_create(parent=in_f.parent, name=name + suffix, type_of_text=type)
        fh = open(out_f, "rb")
        file_obj = File(fh, name=name + suffix)
        new_obj = Filer_File.objects.create(original_filename=name + suffix,
                                            file=file_obj)

        new_obj.folder = folder
        new_obj.save()
        attachment.file = new_obj
        attachment.save()
        logging.debug(f"Created:{attachment}")
    return attachment


