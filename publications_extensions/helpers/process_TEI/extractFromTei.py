import uuid

from bs4 import BeautifulSoup
from django.conf import settings
import os,os.path
from lxml import etree
from filer.models import File as Filer_File, Folder
from publications.models.attachment import Attachment, AttachmentType
from django.core.files import File
import logging
logger = logging.getLogger(__name__)

def extract_body_abstract(in_f):
    """

    :param in_f: django pdfattachment or pk of django pdfattachment should contain a pdf at file object für input
    :param mode:
    :return:
    """
    if not isinstance(in_f,Attachment):
        in_f = Attachment.objects.get(pk=in_f)

    if not in_f.type_of_text.name == "TEI" and not  in_f.type_of_text.name == "XML":
        raise ValueError(f"Attachment is of type {in_f.type} it has to be either TEI or xml.")

    if in_f.file is None:
        raise ValueError(f"Attachment has no file attached")

    in_f_path = in_f.file.path

    os.makedirs(settings.TEMP_PATH, exist_ok=True)
    out_f_abstract = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn + "_abstract")
    out_f_body = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn + "_body")


    xml = etree.parse(in_f_path)
    abstract = xml.xpath("//tei:abstract",
                         namespaces={"tei": "http://www.tei-c.org/ns/1.0"})
    body = xml.xpath("//tei:body",
                     namespaces={"tei": "http://www.tei-c.org/ns/1.0"})

    ##crete abstract
    abstract_txt = ""
    for a in abstract:
        soup = [x.text for x in BeautifulSoup(etree.tostring(a))]
        abstract_txt += " ".join(soup)

    with open(out_f_abstract,"w",encoding="utf-8") as outf:
        outf.write(abstract_txt)

    ##create abstract attachment
    name = in_f.name
    if not name:
        raise ValueError(f" Attachement {in_f.pk} has no name!")
    suffix = ".txt"

    type= AttachmentType.objects.get(name="abstract")
    attachment, created = Attachment.objects.get_or_create(parent=in_f.parent, name=name + suffix, type_of_text=type)
    fh = open(out_f_abstract, "rb")
    file_obj = File(fh, name=name + suffix)
    new_obj = Filer_File.objects.create(original_filename=name + suffix,
                                        file=file_obj)

    new_obj.folder = Folder.objects.get(name="abstract")
    new_obj.save()
    attachment.file = new_obj
    attachment.save()
    logger.debug(f"Created:{attachment}")
    attachment_abstract = attachment

    ## create body
    body_txt = ""
    for a in body:
        soup = [x.text for x in BeautifulSoup(etree.tostring(a))]
        body_txt += " ".join(soup)

    with open(out_f_body, "w", encoding="utf-8") as outf:
        outf.write(body_txt)

    type = AttachmentType.objects.get(name="body")
    attachment, created = Attachment.objects.get_or_create(parent=in_f.parent, name=name + suffix, type_of_text=type)
    fh = open(out_f_body, "rb")
    file_obj = File(fh, name=name + suffix)
    new_obj = Filer_File.objects.create(original_filename=name + suffix,
                                        file=file_obj)

    new_obj.folder = Folder.objects.get(name="body")
    new_obj.save()
    attachment.file = new_obj
    attachment.save()
    logger.debug(f"Created:{attachment}")
    attachment_body = attachment

    return attachment_body, attachment_abstract

