import logging
import os,os.path

import requests
from django.conf import settings
from django.core.management import BaseCommand
import os
import subprocess
import uuid

from django.core.files import File
from publications.models.attachment import Attachment, AttachmentType, PDFAttachment
from filer.models import File as Filer_File, Folder
#GROBID_URL = "http://dw3.mpiwg-berlin.mpg.de:8070/api/"
#TEMP_PATH= "/tmp/pdf2django"
import logging
logger = logging.getLogger(__name__)


def processFile_grobid(in_f):

    if isinstance(in_f,int):
        in_f = PDFAttachment.objects.get(pk=in_f)

    if in_f.file is None:
        logger.error(f" no file attached to {in_f}")
        return
    file_path = in_f.file.path

    os.makedirs(settings.TEMP_PATH,exist_ok=True)

    out_f = os.path.join(settings.TEMP_PATH,uuid.uuid4().urn)

    with open(file_path, "rb") as inf:
         pdf = inf.read()

    res = requests.post(settings.GROBID_URL + "processFulltextDocument", files={"input": pdf})
    if not res.status_code == 200:
         logger.error("Error: %s" % file_path)
         return None
    else:
         with open(out_f, "wb") as outf:
             outf.write(res.content)
         logger.info("ok: %s" % outf)

    suffix = ".xml"
    name = in_f.file.original_filename
    type = AttachmentType.objects.get(name="TEI")
    folder = Folder.objects.get(name="TEI")
    attachment, created = Attachment.objects.get_or_create(parent=in_f.parent, name=name + suffix, type_of_text=type)
    fh = open(out_f, "rb")
    file_obj = File(fh, name=name + suffix)
    new_obj = Filer_File.objects.create(original_filename=name + suffix,
                                        file=file_obj)

    new_obj.folder = folder
    new_obj.save()
    attachment.file = new_obj
    attachment.save()
    logging.debug(f"Created:{attachment}")


    return attachment




