import io
import json

import tarfile
import uuid

import lxml
import spacy
from celery import shared_task, task
from django.conf import settings
from django.contrib.auth.models import User

from publications.models import Type
from publications.models.attachment import AttachmentType, ProcessingType, FormatType
from publications.models.collection import Collection
from publications_extensions.helpers.process_TEI.extractFromTei import extract_body_abstract
from publications_extensions.helpers.process_pdf import pdf2text, pdf2grobid
import logging

from publications_extensions.helpers.process_pdf.pdfUnite import unite
from publications_extensions.helpers.process_txt.createBursts import createBurst
from publications_extensions.helpers.process_txt.createWordperDate import createWord_Date
from publications_extensions.helpers.process_txt.createYearCorpora import createYearCorpora
from publications_extensions.helpers.process_txt.nlp import processNLP

logger = logging.getLogger(__name__)
import os,os.path

@shared_task(track_started=True)
def _extraction_collection_pdf(collection, mode ,pdf_to_use= "pdf", recreate = False):
    #print(collection)
    collection = Collection.objects.get(pk=collection)
    returnValue = []
    returnAttms = []
    returnValue.append(f"Mode  {mode}: Collection {collection.name} has {collection.items.all().count()} items.")
    for item in collection.items.all():
        if item.has_pdf:
            returnValue.append(f"Processing: {item.title}.")
            if pdf_to_use.lower() == "pdf":
                pdfs = item.pdfattachment_set.all()
            else:
                tp = AttachmentType.objects.get(name = pdf_to_use)
                pdfs = item.attachment_set.filter(type_of_text = tp)
            returnValue.append(f"Found {len(pdfs)} pdfs. ({recreate})")
            for pdf in pdfs:
                if mode == "txt":
                    if (not item.has_pdf_text) or recreate:
                        attachment = pdf2text.transform(pdf)
                        if not attachment:
                            returnValue.append(f"Could not create txt for {item}")
                        else:
                            item.save()  # just to update the counters
                            returnAttms.append(item)
                            returnValue.append(f"created txt for {item}")

                elif mode == "xml":
                    returnValue.append(
                        f"has xml: {item.has_xml}")
                    if (not item.has_xml) or recreate:
                        attachment = pdf2text.transform(pdf, mode=mode)
                        if not attachment:
                            returnValue.append(f"Could not create xml for {item}")
                        else:
                            item.save()  # just to update the counters
                            returnAttms.append(item)
                            returnValue.append(f"created txt for {item}")
                elif mode == "grobid":
                    if (not item.has_TEI) or recreate:
                        attachment = pdf2grobid.processFile_grobid(pdf)
                        if not attachment:
                            returnValue.append(f"Could not create TEI for {item}")
                        else:
                            item.save()  # just to update the counters
                            returnAttms.append(item)
                            returnValue.append(f"created tei for {item}")
    collection.save(count=True)

    return {"text": returnValue,
            "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}
    return returnValue

@shared_task(track_started=True)
def _extract_collection_tei(collection):
    #print(collection)
    collection = Collection.objects.get(pk=collection)
    returnValue = []
    returnAttms = []
    cnt = 0
    for item in collection.items.all():
        if item.has_TEI:
            tei =AttachmentType.objects.get(name="TEI")
            txts = item.attachment_set.filter(type_of_text=tei)
            for txt in txts:
                    if not item.has_abstract or not item.has_body:
                        try:
                            attachment_body, attachment_abstract = extract_body_abstract(txt)
                            cnt += 1
                        except ValueError:
                            returnValue.append(f"Could not create body and abstract for {item} -- Value Error (not tei)")
                            continue
                        except lxml.etree.Error:
                            fn = txt.file.path
                            returnValue.append(f"Could not create body and abstract for {item}  -- xmlerror in {fn}!")
                            continue

                        if not attachment_body:
                            returnValue.append(f"Could not create body for {item} -- Unknown Error")
                        else:
                            returnAttms.append(item)
                            returnValue.append(f"created body for {item}")
                        if not attachment_abstract:
                            returnValue.append(f"Could not create abstract for {item} -- Unknown Error")
                        else:
                            returnAttms.append(item)
                            returnValue.append(f"created abstract for {item}")
                        item.save()  # just to update the counters
    collection.save(count=True)
    returnValue.append(f"Created {cnt} attachments!")
    return {"text": returnValue,
            "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}
    return returnValue

@shared_task(track_started=True)
def _export_attachments_collection_items(collection, type_of_text, type_of_processing):#, format):
    #print(collection)
    collection = Collection.objects.get(pk=collection)

    metadata = {}
    urn = uuid.uuid4().urn
    fn = f"{collection.name}_{type}_{urn}.tgz"
    fn = os.path.join(settings.TEMP_PATH,fn)

    tf = tarfile.open(fn,"w:gz")
    for item in collection.items.all():
        att = AttachmentType.objects.get(name=type_of_text)
        ptt = ProcessingType.objects.get(name=type_of_processing)
        #format = FormatType.objects.get(name=format)
        txts = item.attachment_set.filter(type_of_text=att, type_of_processing = ptt).order_by("-updated_at")

        for txt in txts:
            if txt.file is None:
                logger.debug(f"No file at {txt}, try later one by date!")
                continue # try next one
            fh = txt.file.file
            content = fh.read()
            s = io.BytesIO()
            s.write(content)
            s.seek(0)
            ti = tarfile.TarInfo(name=txt.name)
            ti.size = len(s.getvalue())

            tf.addfile(tarinfo=ti, fileobj=s)

            metadata[txt.name] = {"title": item.title,
                                  "year" : item.year,
                                  "date" : item.date}
            break

    s = io.BytesIO()
    s.write(json.dumps(metadata).encode("utf-8"))
    s.seek(0)
    tarinfo = tarfile.TarInfo(name="metadata.json")
    tarinfo.size = len(s.getvalue())
    tf.addfile(tarinfo=tarinfo, fileobj=s)

    tf.close()
    return {"text": [fn],
            "related_objects": []}


@shared_task(track_started=True)
def _export_collection_attachments(collection, type_of_text, type_of_processing, format):
    #print(collection)
    collection = Collection.objects.get(pk=collection)

    metadata = {}
    urn = uuid.uuid4().urn
    fn = f"{collection.name}_{type_of_text}_{urn}.tgz"
    fn = os.path.join(settings.TEMP_PATH,fn)

    tf = tarfile.open(fn,"w:gz")

    att = AttachmentType.objects.get(name=type_of_text)
    ptt = ProcessingType.objects.get(name=type_of_processing)
    format = FormatType.objects.get(name=format)
    txts = collection.collectionattachment_set.filter(type_of_text=att, type_of_processing = ptt, format = format)

    for txt in txts:
        if txt.file is None:
            logger.debug(f"No file at {txt}, try later one by date!")
            continue # try next one
        fh = txt.file.file
        content = fh.read()
        s = io.BytesIO()
        s.write(content)
        s.seek(0)
        ti = tarfile.TarInfo(name=txt.name)
        ti.size = len(s.getvalue())

        tf.addfile(tarinfo=ti, fileobj=s)



    tf.close()
    return {"text": [fn],
            "related_objects": []}



@shared_task(track_started=True)
def _NLP_collection_pdf(collection, mode, type_of_text, lang,start=None,end=None, update_only=False):
    """
    Transform the attachment -- always only the newest attachment of the defined type
    is processed for each item

    :param collection:  pk of a collection
    :param mode: nlp mode to be choosen see @processNLP
    :param type: which type of attachment to be used
    :param lang: the spacy language file to be used
    :return:
    """
    #print(collection)
    collection = Collection.objects.get(pk=collection)
    returnValue = []
    NLP = spacy.load(lang)
    NLP.max_length=100000000
    items = collection.items.all()

    if (start is not None) and (end is None):
        items = items[start:]
    if (start is not None) and (end is not None):
        items = items[start:end]


    for item in items:
        try:
            att = AttachmentType.objects.get(name=type_of_text)
        except AttachmentType.DoesNotExist:
            raise AttachmentType.DoesNotExist(f"{type_of_text}")
        txts = item.attachment_set.filter(type_of_text=att).order_by("-updated_at")
        # go throw all attachment of the type ordered by date
        for txt in txts:
            if txt.file is None:  # go one if it empty
                logger.debug(f"No file at {txt}!")
                continue

            returnValue.append(processNLP(txt, mode=mode, NLP=NLP, update_only=update_only)) #process this
            break #stop now (only the newest attachment of this type is processed)

    return {"text": returnValue,
            "related_objects": []}



@shared_task(track_started=True)
def _createYearCorpora(collection,user= None, lang="en",type_of_text="abstract",format="pickle" , store_as_attachment=True):
    user = User.objects.get(username = user)
    collection = Collection.objects.get(pk=collection)
    returnValue, returnAttms = createYearCorpora(collection,lang=lang,type_of_text=type_of_text, format = format, store_as_attachment=True, user = user)
    return {"text": returnValue,
             "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}


@shared_task(track_started=True)
def _createWord_Date(collection, lang, user , typ_of_text,  start ,end ,format, threshold):
    user = User.objects.get(username=user)
    returnValue , returnAttms= createWord_Date(collection, lang, user, typ_of_text, start, end, format, threshold)
    return {"text": returnValue,
            "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}


@shared_task(track_started=True)
def _createBurst(all_r,user,s, gam, only_years):
    user = User.objects.get(username=user)
    returnValue, returnAttms = createBurst(all_r,user,s, gam, only_years=only_years)
    return {"text": returnValue,
            "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}

@shared_task(track_started = True)
def _pdfUnite(p_s_r):
    returnValue, returnAttms = unite(p_s_r)
    return {"text": returnValue,
            "related_objects": [(attm.pk, attm.__class__.__name__) for attm in returnAttms]}
