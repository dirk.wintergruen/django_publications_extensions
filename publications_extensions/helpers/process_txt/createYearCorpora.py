import pickle
import tarfile
import uuid

from django.conf import settings
from django.core.files import File
from textacy import Corpus

from publications.models.attachment import AttachmentType, CollectionAttachment, FormatType, ProcessingType
from publications.models.collection import Collection
import logging
logger = logging.getLogger(__name__)

import re
import os,os.path
from filer.models import  File as Filer_File, Folder


def clean(txt):
    txt = txt.replace("•"," ")
    txt= re.sub(r"([;|\+|\'|\\|\"|\!|\.|\-|,|/|\)|\(])([ÄÖÜäöüa-zA-Z])",r" \1 \2", txt)
    return txt


def getMD(item):
    return dict(title = item.title, year = item.year, date = item.date)


def createYearCorpora(collection,lang="en",type_of_text="abstract",format="pickle",
                      store_as_attachment = True,
                      user = None,
                      only_latest_version=True):
    """

    :param collection:
    :param lang:
    :param type_of_text:
    :param format:
    :param store_as_attachment:
    :param user:
    :param only_latest_version: if set to true (default) then only the latest version of an attachment is choosen
    if there are more than one the same type.
    :return:
    """
    formats = ["pickle","corpus"]
    if not format in formats:
        raise ValueError(f"value for format not in {formats}")
    returnValue = []
    returnAttms = []
    if not isinstance(collection,Collection):
        collection = Collection.objects.get()
    years = set(i.year for i in collection.items.all())
    corpora = {}
    corpus_files = {}
    urn = uuid.uuid4().urn
    outfolder = os.path.join(settings.TEMP_PATH,urn)
    os.makedirs(outfolder,exist_ok=True)
    for y in years:
        corpus = Corpus(lang=lang)
        corpus.spacy_lang.max_length = 20000000
        for item in collection.items.filter(year=y):
            tp = AttachmentType.objects.get(name=type_of_text)
            attms = item.attachment_set.filter(type_of_text=tp).order_by("-updated_at")
            if only_latest_version and len(attms) > 0:
                attms = [attms[0]]
            for attm in attms:
                if not attm.file:
                    returnValue.append(f"no file: {attm.name}")
                    continue
                with open(attm.file.path, "r", encoding="utf-8") as inf:
                    txt = inf.read()
                    if len(txt.strip()) == 0:
                        logger.info(f"no text: {attm.name}")
                        returnValue.append(f"no text: {attm.name}")
                        continue
                    txt = clean(txt)
                md = getMD(item)
                md["id"] = attm.name
                corpus.add_record((txt, md))
        returnValue.append(f"created year: {y}")
        corpora[y] = corpus


        if format == "pickle":
            outfn = os.path.join(outfolder,f"{y}.pickle")
            corpus_files[y] = outfn
            with open(outfn,"wb") as outf:
                pickle.dump(corpus,outf)
                returnValue.append(f"save: {outfn}")
        elif format =="corpus":
            if len(corpus) == 0:
                continue
            outfn = os.path.join(outfolder,f"{y}.corpus")
            corpus_files[y] = outfn
            corpus.save(outfn)
            returnValue.append(f"save: {outfn}")

        if store_as_attachment:

            name = f"{collection.name}_{y}.{format}"
            folder, created = Folder.objects.get_or_create(name = "corpora")
            fh = open(os.path.join(outfn), "rb")
            file_obj = File(fh, name=name)
            new_obj = Filer_File.objects.create(original_filename=name,
                                                file=file_obj)
            new_obj.folder = folder
            new_obj.save()

            attm = CollectionAttachment(name=name)
            ptp,created = ProcessingType.objects.get_or_create(name=f"corpus_year")
            attm.type_of_processing = ptp
            attm.type_of_text = tp
            attm.year = y
            format_type, created = FormatType.objects.get_or_create(name=format)
            attm.format = format_type
            attm.lang = lang
            attm.parent = collection
            attm.save()
            attm.file = new_obj
            if user:
                attm.user = user
            attm.save()
            returnAttms.append(attm)


    outfn = os.path.join(outfolder, f"corpora_collection_{collection.name}_{lang}_{type_of_text}.tgz")
    tf = tarfile.open(outfn,"w:gz")
    for y,path in corpus_files.items():
        #fh = open(path)
        #ti = tarfile.TarInfo(name=f"{y}.pickle")
        #ti.size = fh.si
        #tf.addfile(ti,fh)
        if format == "pickle":
            tf.add(path,f"{y}.pickle")
        elif format == "corpus":
            tf.add(path, f"{y}.corpus")
    tf.close()

    if store_as_attachment:

        name = f"corpora_collection_{collection.name}_{lang}_{type_of_text}.tgz"
        folder, created  = Folder.objects.get_or_create(name="corpora")
        fh = open(os.path.join(outfn), "rb")
        file_obj = File(fh, name=name)
        new_obj = Filer_File.objects.create(original_filename=name,
                                            file=file_obj)
        new_obj.folder = folder
        new_obj.save()

        attm = CollectionAttachment(name=name)
        ptp, created = ProcessingType.objects.get_or_create(name=f"corpus")
        attm.type_of_processing = ptp
        attm.type_of_text = tp
        attm.lang = lang
        format_type, created = FormatType.objects.get_or_create(name="tgz")
        attm.format = format_type
        attm.parent = collection
        attm.save()
        attm.file = new_obj
        if user:
            attm.user = user
        attm.save()
        returnAttms.append(attm)
    returnValue.append(outfn)
    return returnValue, returnAttms



