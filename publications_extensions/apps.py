from django.apps import AppConfig


class PublicationsExtensionsConfig(AppConfig):
    name = 'publications_extensions'
