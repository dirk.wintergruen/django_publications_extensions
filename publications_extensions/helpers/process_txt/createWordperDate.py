from django.conf import settings
from django.core.files import File

from burstanalysis.analyseCorpus import Analyser
from publications.models import Collection
from publications.models.attachment import AttachmentType, CollectionAttachment, ProcessingType, FormatType
from filer.models import  File as Filer_File, Folder
import os.path
import uuid
import pickle
import logging
logger = logging.getLogger(__name__)

def createWord_Date(collection, lang, user, typ_of_text, start ,end, format ,threshold = 50):


    tp = AttachmentType.objects.get(name=typ_of_text)
    ptf = ProcessingType.objects.get(name="corpus_year")
    if  not isinstance(collection ,Collection):
        collection = Collection.objects.get(pk=collection)
    attms = CollectionAttachment.objects.filter(type_of_text=tp ,lang=lang, parent = collection, type_of_processing = ptf)
    corpus = {attm.name : attm.file.file.path for attm in attms}
    analyser = Analyser(corpus,lang=lang, start=start,end=end)

    data, wordcounts = analyser.createYearMonthDataset()

    name = f"{collection.name}_{start}_{end}_{typ_of_text}_year_month_words.{format}"
    folder, created = Folder.objects.get_or_create(name="corpus_date")

    if format == "pickle":
        fn = os.path.join(settings.TEMP_PATH ,uuid.uuid4().urn) + ".pickle"
        with(fn ,"wb") as outf:
            pickle.dump(data ,outf)

    elif format == "xlsx":
        fn = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn) + ".xlsx"
        data.to_excel(fn)
    elif format == "csv":
        fn = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn) + ".csv"
        data.to_csv(fn)


    fh = open(os.path.join(fn), "rb")
    file_obj = File(fh, name=name)
    new_obj = Filer_File.objects.create(original_filename=name,
                                        file=file_obj)
    new_obj.folder = folder
    new_obj.save()

    attm = CollectionAttachment(name=name)
    ptp,created = ProcessingType.objects.get_or_create(name="data")
    format_type, created = FormatType.objects.get_or_create(name=format)
    attm.type_of_text = tp
    attm.type_of_processing = ptp
    attm.format= format_type
    attm.lang = lang
    attm.parent = collection
    attm.save()
    attm.file = new_obj
    if user:
        attm.user = user
    attm.save()
    date_attm = attm

    returnValue =  [f"Created {attm}"]
    returnAttms = [attm]

    #noew create all_r

    analyser.createListOfUniqueWords(threshold=threshold)

    name = f"{collection.name}_{start}_{end}_{typ_of_text}_{threshold}_wordcounts.csv"

    fn = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn) + ".csv"

    analyser.wordCounts.to_csv(fn)

    fh = open(os.path.join(fn), "rb")
    file_obj = File(fh, name=name)
    new_obj = Filer_File.objects.create(original_filename=name,
                                        file=file_obj)
    new_obj.folder = folder
    new_obj.save()

    attm = CollectionAttachment(name=name)
    ptp, created = ProcessingType.objects.get_or_create(name=f"wordcounts")
    format_type, created = FormatType.objects.get_or_create(name="csv")
    attm.type_of_text = tp
    attm.lang = lang
    attm.type_of_processing = ptp
    attm.format = format_type

    attm.parent = collection
    attm.save()
    attm.file = new_obj
    if user:
        attm.user = user
    attm.derived_from_collection_attachment.add(date_attm)
    attm.save()
    wordcount_attm = attm

    returnValue += [f"Created {attm}"]
    logger.debug("Now create all_r")
    name = f"{collection.name}_{start}_{end}_{typ_of_text}_{threshold}_all_r.pickle"

    all_r = analyser.createAllPresentWordsPerYearMonth(data)

    fn = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn) + ".pickle"
    with open(fn, "wb") as outf:
        pickle.dump(all_r, outf)

    fh = open(os.path.join(fn), "rb")
    file_obj = File(fh, name=name)
    new_obj = Filer_File.objects.create(original_filename=name,
                                        file=file_obj)
    new_obj.folder = folder
    new_obj.save()

    attm = CollectionAttachment(name=name)
    ptp, created = ProcessingType.objects.get_or_create(name=f"all_r_{threshold}")
    format_type, created = FormatType.objects.get_or_create(name="pickle")
    attm.type_of_text = tp
    attm.lang = lang
    attm.type_of_processing = ptp
    attm.format = format_type

    attm.parent = collection
    attm.save()
    attm.file = new_obj
    if user:
        attm.user = user
    attm.derived_from_collection_attachment.add(date_attm)
    attm.derived_from_collection_attachment.add(wordcount_attm)
    attm.save()

    returnValue += [f"Created {attm}"]
    returnAttms.append(attm)
    return returnValue, returnAttms