import time

from django.conf import settings

from publications.models.attachment import Attachment, AttachmentType
import os,os.path
import uuid
import spacy
import io
from filer.models import File as Filer_File, Folder
from django.core.files import File
import logging
logger = logging.getLogger(__name__)

ACCEPTEDMODES=["lemmatize","tokenize"]
def processNLP(in_f,mode="lemmatize",NLP=None,update_only=False):
    time_s = time.time()

    name = in_f.name
    if not name:
        raise ValueError(f" Attachement {in_f.pk} has no name!")
    suffix = ".txt"
    at_type = "%s_%s" % (mode, in_f.type.name)
    type = AttachmentType.objects.get(name=at_type)
    attachment, created = Attachment.objects.get_or_create(parent=in_f.parent, name=name + suffix, type_of_text=type)

    if update_only and not created:
        logger.info(f"Exists: {attachment} - {time.time() - time_s} skippping")
        return f"Exists: {attachment} - {time.time() - time_s} skipping"

    if not isinstance(in_f, Attachment):
        in_f = Attachment.objects.get(pk=in_f)

    #if not in_f.type.name == "TEI" and not in_f.type.name == "XML":
    #    raise ValueError(f"Attachment is of type {in_f.type} it has to be either TEI or xml.")

    in_f_path = in_f.file.path
    logger.debug(f"Analyzing: {in_f_path}")
    os.makedirs(settings.TEMP_PATH, exist_ok=True)
    #out = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn + "_lemma")

    #NLP = spacy.load(lang)

    with open(in_f_path,"r",encoding="utf-8") as inf:
        txt = inf.read()

    txt=  NLP(txt)

    if mode == "lemmatize":
        words = [x.lemma_ for x in txt]
    elif mode == "tokenize":
        words = [str(x) for x in txt]

    else:
        raise ValueError(f"Mode not in {','.join(ACCEPTEDMODES)} !")


    ##create abstract attachment
    #fh = open(out_f_abstract, "rb")

    fh = io.StringIO()
    fh.seek(0)
    for w in words:
        fh.write(w + "||")

    file_obj = File(fh, name=name + suffix)
    new_obj = Filer_File.objects.create(original_filename=name + suffix,
                                        file=file_obj)

    new_obj.folder = Folder.objects.get(name=at_type)
    new_obj.save()
    attachment.file = new_obj
    attachment.save()
    #logger.debug(f"Created:{attachment}")
    in_f.derived_from.add(attachment)
    in_f.save()
    logger.info(f"Created: {attachment} - {time.time() - time_s}")

    return f"Created: {attachment} - {time.time() - time_s}"
