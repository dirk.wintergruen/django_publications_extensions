from django.core.management import BaseCommand

import logging

try:
    from documents.models import Source, Page, ALLOWED_TYPES
except:
    print("For the management of pages the documentManager modul is needed, but it is not properly installed here.")
from publications.models import Publication
from publications_extensions.helpers import filesHandler
from publications_extensions.models import PublicationSourceReference
import os
from filer.models import Folder

class Command(BaseCommand):
    help = 'attach pages from a folder to publication'


    def add_arguments(self, parser):
        parser.add_argument('--publication_id', type=int,
                            help="django id of the publication",
                            required=True)
        parser.add_argument('--path', type=str,
                            default = None,
                            help="path to the folder containing the files",
                            required=True)

        parser.add_argument('--type', type=str,
                            default="pdf-text",
                            help="type of the pages")

        parser.add_argument('--source_id', type=int,
                            default=None,
                            help="id of the source attached to the publication, required if more than one is attached.")




    def handle(self, *args, **options):
        logging.basicConfig(level=logging.DEBUG)

        typ = options["type"]
        if not typ in ALLOWED_TYPES:
            print(f"ERROR: Type has to be in {ALLOWED_TYPES}")
            return

        publication = Publication.objects.get(pk=options["publication_id"])

        #find reference to a source

        source_references = PublicationSourceReference.objects.filter(publication=publication)

        # first upload all pages

        foldername = f"pages_{publication.pk}"

        folder_pages,created = Folder.objects.get_or_create(name="pages")
        folder,created = Folder.objects.get_or_create(name=foldername)
        if created:
            folder.save()
            folder.folder = folder_pages
            #folder_pages.append(folder)
            #folder_pages.save()

        pages = uploadPages(options["path"],foldername,typ)

        if len(source_references) == 0: #there isn't one yet
            source = Source()
            source.save()
            source_reference = PublicationSourceReference(publication=publication,source=source)
            source_reference.save()
        elif len(source_references) == 1: #exactly one
            source_reference = source_references[0]
            source = source_reference.source
        else:
            if options["source_id"] is None:
                print("There is more than one source attached to the publication. You have to set --source_id")
                return False

            source_reference = None
            for s_r in source_references:
                if s_r.source.pk == options["source_id"]:
                    source_reference = s_r
                    source = source_reference.source
                    break

            if source_reference is None:
                print(f"There is no source with id {options['source_id']} attached to the publication with id {options['publication_id']}")
                print(f"will not do anything!")
                return False

        for p in pages:
            p.setSource(source)
            p.type = typ

        source.save()
        print(f"Created source {source.pk} attached to {publication.pk} linked via {source_reference.pk}.")

def uploadPages(path,foldername,type):
    """upload pages from path and create a list of page objects"""
    ret = []
    for root,dirs,files in os.walk(path):
        for f in files:
            page = uploadPage(os.path.join(root,f),f,foldername,type)
            ret.append(page)
    return ret

def uploadPage(path,filename,foldername,type):
    """upload a page from path and create a page object"""


    file = filesHandler.uploadOrGetExistingFile(path,
                           filename,
                           foldername,
                           always_upload=False)


    page,created = Page.objects.get_or_create(path=file.path)
    page.type = type
    if created:
        page.save()
    return page






