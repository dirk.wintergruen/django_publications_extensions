from django.core.files import File
from filer.models import File as Filer_File, Folder
import logging
logger = logging.getLogger(__name__)

def uploadOrGetExistingFile(path,
                            filename,
                            folder_name,
                            always_upload = False):
    """upload a file from path or give the existing file object back"""

    fh = open(path, "rb")

    file_obj = File(fh, name=filename)
    new_obj = Filer_File.objects.create(original_filename=filename,
                                     file=file_obj)

    sha1_neu = new_obj.sha1
    logger.debug(f"created a new object {new_obj}")
    if not always_upload:  # don't save the new object if we have already a files with the same sha
        logger.debug(f"not always upload selected")
        logger.debug(f"check if it already exists, with the same sha1_neu")

        objs_old = Filer_File.objects.filter(sha1=sha1_neu).exclude(
            id=new_obj.id)  # same object exists already
        if len(objs_old) > 0:
            new_obj.delete()
            del new_obj
            logger.debug(f"already exists, will use this one. delete the new object again.")
            new_obj = objs_old[0]
        elif len(objs_old) == 0:
            logger.info("upload new object")

            fld, created = Folder.objects.get_or_create(name=folder_name)
            new_obj.folder = fld
            new_obj.save()

        else:
            logger.warning("Objects exists more than once")

            for i in objs_old:
                logger.warning(i.label)
            logger.warning(f"I choose the first!")
            new_obj.delete()
            del new_obj
            new_obj = objs_old[0]
            fld = Folder.objects.get(name=folder_name)
            new_obj.folder = fld
    else:  # always_upload
        logger.debug(f"always upload selected")
        fld, created = Folder.objects.get_or_create(name=folder_name)
        new_obj.folder = fld
        new_obj.save()

    return new_obj