"""generate graphs of terms around a given keyword"""
import os, django
import sys
sys.path.append("/media/data/science_nature_cms/cms")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "exo_online.settings")
django.setup()

import os

import igraph

from publications.models import Collection

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
import pandas as pd
import spacy
import textacy
import textacy.vsm
import textacy.tm
import textacy.preprocessing.normalize
import textacy.text_utils
import matplotlib.pyplot as plt
import pickle


def get_docs_from_collection(pk):
    col = Collection.objects.get(pk=pk)
    cols = col.collectionattachment_set.all()
    corpus = textacy.Corpus.load('de_core_news_lg', cols[0].file.path)
    return corpus

def findKeyword(doc,keyword):
    doc_proc = textacy.preprocessing.normalize.normalize_whitespace(str(doc))
    doc_proc = textacy.preprocessing.normalize_hyphenated_words(doc_proc)
    doc_proc = textacy.preprocessing.normalize.normalize_unicode(doc_proc)
    de = textacy.load_spacy_lang("de")
    de.max_length = 20000000
    doc = textacy.make_spacy_doc(doc_proc, lang=de)

    terms = list(textacy.extract.ngrams(doc, 1, filter_stops=True, filter_punct=True, filter_nums=True))

    findings = []
    for num, term in enumerate(terms):
        if keyword.lower() in str(term.lemma_).lower():
            findings.append((num, term.lemma_))

    return terms,findings


def findNeighbourhoodsAndWords(terms,findings):
    umgeb = {}
    rel_words = [x.lemma_ for x in terms]
    for p, f in findings:
        umgeb[(p, f)] = rel_words[p - 10:p + 10]

    words = set()
    for u in umgeb.values():
        words.update((str(x) for x in u))

    return umgeb,words

def generateGraphsFromNeigbourhoods(umgeb,words):
    nodes = list(words)
    graph = igraph.Graph()
    graph.add_vertices(len(nodes))

    graph.vs["name"] = list(nodes)
    graph.vs["label"] = [x[:20] for x in graph.vs["name"]]

    core_nodes = [str(x[1]) for x in umgeb] #nodes which contain the keyword
    cores = [x in core_nodes for x in graph.vs["name"]]
    graph.vs["core"] = cores

    edges = []
    weights = []
    for p_f, u in umgeb.items():
        p, f = p_f
        s = nodes.index(str(f))
        for n in u:
            e = nodes.index(str(n))
            edges.append((s, e))
            weights.append(abs(u.index(str(f)) - u.index(n)))
    graph.add_edges(edges)
    graph.es["weigth"] = weights
    return graph


def main():
    

    
    pk = 2
    docs = get_docs_from_collection(pk)
    for d in docs:
        fn = d._.meta["id"]
        print(fn)
        terms, findings = findKeyword(d,"forschung")
        umgeb, words = findNeighbourhoodsAndWords(terms,findings)
        graph = generateGraphsFromNeigbourhoods(umgeb,words)
        graph.write_gml(f"data/{fn}.gml")
    print("DONE")


de = textacy.load_spacy_lang("de")
de.max_length = 20000000
    
main()
