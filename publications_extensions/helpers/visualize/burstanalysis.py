import pandas
import numpy as np
import matplotlib.pyplot as plt
from plotly.offline import plot as iplot
import seaborn as sns
import cufflinks
cufflinks.go_offline()
from plotly.offline import plot as iplot

class BurstPlotter(object):

    def __init__(self,typ,all_bursts,word_list, all_r,d, s, gam, start_year = 0, end_year = 9999):
        # format plots
        sns.set(style='white', context='notebook', font_scale=1.5,
                rc={'font.sans-serif': 'DejaVu Sans', 'lines.linewidth': 2.5})

        # create a custom color palette
        self.palette21 = ['#21618C', '#3498DB', '#AED6F1', '#00838F', '#00BFA5',
                     '#F1C40F', '#F9E79F', '#E67E22', '#922B21', '#C0392B',
                     '#E6B0AA', '#6A1B9A', '#8E44AD', '#D7BDE2', '#196F3D',
                     '#4CAF50', '#A9DFBF', '#4527A0', '#7986CB', '#555555',
                     '#CCCCCC']

        # create a color map
        self.blog_blue = '#64C0C0'
        self.blue_cmap = sns.light_palette(self.blog_blue, as_cmap=True)
        self.typ = typ

        self.all_bursts = all_bursts
        self.word_list = word_list


        if typ == "year":
            self.all_r = all_r.groupby("_year").sum()
        else:
            self.all_r = all_r

        if typ == "year":
            d = d.groupby("_year").sum()
            d = d[d.index > start_year]
            d = d[d.index < end_year]
            d = d.sort_index()
        else:
            d = d[[x[0] > start_year for x in d.index]].sort_index()
            d = d[[x[0] < end_year for x in d.index]].sort_index()

        self.d = d

    def plot_d(self):
        x = self.d.sort_index().iplot(asFigure=True)
        div = iplot(x, output_type='div', include_plotlyjs=True)
        return div
