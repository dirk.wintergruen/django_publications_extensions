from django.shortcuts import render
from django.urls import path
from publications_extensions import views
app_name = "publicationsExtension"
urlpatterns = [
    path('publication/<int:pk>', views.publicationView,name="publicationView"),
    path('publication/zotero/<str:zoterokey>', views.publicationView,name="publicationView"),
    path('pdf2text/<int:pk>', views.createMissingTxts_in_CollectionFromPDF, name="pdf2text"),
    path('pdf2text/', views.createMissingTxts_in_CollectionFromPDF, name="pdf2text"),
    path('tei2abstract_body/<int:pk>', views.createMissingAbstract_Bodies_in_CollectionFromTEI, name="tei2abstract_body"),
    path('tei2abstract_body/', views.createMissingAbstract_Bodies_in_CollectionFromTEI, name="tei2abstract_body"),
    path('showalltasks/', views.showAllTasks,name = "showalltasks"),
    path('showtask/<str:taskid>', views.showTask, name = "showtask"),
    path('exportAttachment/<int:pk>/<type>', views.exportAttachmentsOfCollectionItems, name="export_attachment"),
    path('exportAttachment/', views.exportAttachmentsOfCollectionItems, name="export_attachment"),
    path('exportCollectionAttachment/', views.exportCollectionAttachments, name="export_collectionattachment"),
    path('nlp/<int:pk>/<type>', views.processNLP, name="processNLP"),
    path('nlp/', views.processNLP, name="processNLP"),
    path('createCorpora/', views.createCorpora, name="createCorpora"),
    path('createBurst/', views.createBurst, name="createBurst"),
    path('createWordsPerDate/', views.createWordsPerDate, name="createWordsPerDate"),
    path('download/<path:fn>', views.download_from_tmp, name="download"),
    path('plot_d/<int:collection_pk>', views.plot_d, name="plot_d"),
    path('plot_burst/<int:burst_pk>', views.plot_burst, name="plot_burst"),
    path('pdfunite/<int:publication_pk>', views.pdf_unite, name="pdf_unite"),
    path('',views.index,name="index")
    ]

