
import logging
import pickle
import uuid

from django.conf import settings

from burstanalysis.analyseCorpus import Analyser
from publications.models import Collection
from publications.models.attachment import CollectionAttachment, ProcessingType, FormatType
from filer.models import  File as Filer_File, Folder
from django.core.files import  File

logger = logging.getLogger(__name__)
import pandas
import os.path

def createBurst(all_r,user,s, gam,only_years=False):

    if not isinstance(all_r,CollectionAttachment):
        all_r = CollectionAttachment.objects.get(pk=all_r)
    collection = all_r.parent
    datas = all_r.derived_from_collection_attachment.all()
    if len(datas) == 0:
        raise ValueError(f"all_r {all_r} has not data attached!")
    elif len(datas) != 2:
        raise ValueError(f"all_r {all_r} has more than two dataset attached! choose the first.")

    pt_data = ProcessingType.objects.get(name="data")
    pt_wordlist = ProcessingType.objects.get(name="wordcounts")

    wordlist = None
    all_r_data =None
    for data in datas:

        if data.type_of_processing == pt_data:
            data = datas[0]
            if data.format.name == "pickle":
                all_r_data = pandas.read_pickle(data.file.file.path)

            elif data.format.name == "csv":
                all_r_data = pandas.read_csv(data.file.file.path)
            elif data.format.name == "xlsx":
                all_r_data = pandas.read_excel(data.file.file.path)
            else:
                raise ValueError(f"data atachment is of format: {data.format.name}. I cannot handle this")


        if data.type_of_processing == pt_wordlist:
            wordlist = pandas.read_csv(data.file.file.path,squeeze = True, index_col=0,dtype = {"0":"float64"})

    if wordlist is None  or all_r_data is None:
        raise ValueError(f"all_r {all_r} has not wordlist and data attached!")

    top =  all_r.type_of_processing.name.split("_")
    threshold = top[-1]

    analyser = Analyser(None, lang=None)
    analyser.wordCounts =  wordlist
    analyser.threshold = int(threshold)

    with open(all_r.file.file.path,"rb") as inf:
        all_r_df = pickle.load(inf)

    analyser.all_r = all_r_df
    bursts,date_list = analyser.find_bursts(all_r_data,s,gam,only_years=only_years)


    fn = os.path.join(settings.TEMP_PATH, uuid.uuid4().urn) + ".xlsx"
    bursts.to_excel(fn)

    #bursts.to_pickle(os.path.join(settings.TEMP_PATH, uuid.uuid4().urn)  +".pickle")

    name = f"{collection.name}_{s}_{gam}_{all_r.type_of_text.name}_burst.xlsx"
    #with open(fn, "wb") as outf:
    #    pickle.dump(all_r, outf)

    fh = open(os.path.join(fn), "rb")
    file_obj = File(fh, name=name)
    folder, created = Folder.objects.get_or_create(name="bursts")

    new_obj = Filer_File.objects.create(original_filename=name,
                                        file=file_obj)
    new_obj.folder = folder
    new_obj.save()


    attm = CollectionAttachment(name=name)
    if only_years:
        ptp, created = ProcessingType.objects.get_or_create(name=f"burst_{s}_{gam}_year")
    else:
        ptp, created = ProcessingType.objects.get_or_create(name=f"burst_{s}_{gam}_year_month")
    format_type, created = FormatType.objects.get_or_create(name="xlsx")
    attm.type_of_text = all_r.type_of_text
    attm.lang = all_r.lang
    attm.type_of_processing = ptp
    attm.format = format_type
    attm.parent = collection
    attm.save()
    attm.file = new_obj
    if user:
        attm.user = user
    attm.derived_from_collection_attachment.add(all_r)
    attm.save()

    return [f"Attm {attm} created!"],[attm]