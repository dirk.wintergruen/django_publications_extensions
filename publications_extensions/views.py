import matplotlib.pyplot as plt
import matplotlib as mpl

import django_filters

import numpy as np
from plotly.offline import plot as iplot
from celery.result import AsyncResult
from django.conf import settings
from django.core.cache import caches
from django.http import Http404, FileResponse, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.urls import reverse
from django.utils.html import format_html
import django_tables2 as tables
from filer.models import Folder

from publications.models import Publication
from publications.models.attachment import AttachmentType, ProcessingType, FormatType, \
    CollectionAttachment, Attachment ## Attachment importieren, wegen der globals weiterunten
from publications.models.collection import Collection
from publications.models.publication import ATTACHMENTTYPES
from publications_extensions.helpers.visualize.burstanalysis import BurstPlotter
from publications_extensions.helpers.visualize.tools import currentPltToImage
from publications_extensions.models import TaskResult
from publications_extensions.tasks import _extraction_collection_pdf, _extract_collection_tei, \
    _export_attachments_collection_items, _NLP_collection_pdf, _createYearCorpora, _createWord_Date, _createBurst, \
    _export_collection_attachments,_pdfUnite
import pandas

import os, os.path
import logging
logger = logging.getLogger(__name__)

def publicationView(request,pk=None,zoterokey=None):

    if pk and zoterokey:
        raise ValueError("pk or zoterokey has to be set. Not both!")
    if pk:
        publication = get_object_or_404(Publication,pk=pk)
    elif zoterokey:
        publication = get_object_or_404(Publication, zoterokey=zoterokey)
    else:
        raise ValueError("pk or zoterokey has to be set.")

    pdfs =  publication.pdfattachment_set.all()

    attachments = publication.attachment_set.all().order_by("-updated_at").order_by("-type_of_text")

    atms_texts = []
    for atm in attachments:
        try:
            txt =  atm.file.file.read().decode("utf-8")
        except UnicodeDecodeError:
            logger.warning(f"Attachment: {atm} not utf-8!, will try latin-1")
            txt = atm.file.file.read().decode("latin-1")
        if len(txt) > 10000:
            txt = txt[0:10000] +f"\n (abbreviated) Text to long ({len(txt)} characters). Use link above!"
        atms_texts.append({"type":atm.type_of_text,
                            "txt" : txt,
                           "updated_at" : atm.updated_at})

    return render(request, "publicationExtensions/publication.html", {
        "publication": publication,
        "pdfs" : pdfs,
        "attachments" : attachments,
        "atms_texts" : atms_texts
    })



def createMissingTxts_in_CollectionFromPDF(request,pk=None,mode="txt", _pdf_to_use = "pdf", _recreate= "No"):
    """
    Extract texts or html from pdfs in a collecection
    :param request:
    :param pk: pk of a collection (optional, will be overwritten by parameter pk in request
    :param mode: mode of a collection (optional, will be overwritten by parameter mode in request (should be txt, html or grobid)
    :return: redirect either to input form or to showtask
    """

    pk = request.GET.get("pk",pk)
    try:
        int(pk)
    except:
        pk = None

    if pk is None:
        return render(request,"publicationExtensions/pdftotext/chooseCollection.html",
                      {"collections" : Collection.objects.all()})

    collection = get_object_or_404(Collection,pk=pk)
    mode = request.GET.get("mode",mode)
    pdf_to_use = request.GET.get("pdf_to_use", _pdf_to_use)
    _recreate = request.GET.get("recreate", _recreate)

    if _recreate.lower() == "no":
        recreate = False
    else:
        recreate = True


    returnValue = _extraction_collection_pdf.delay(collection.pk, mode,  pdf_to_use= pdf_to_use, recreate = recreate)

    resultObj = TaskResult(user=request.user,key=returnValue.id,
                           taskname = f"createMissingTxts_in_CollectionFromPDF: {collection.name} - {mode} - pdf_to_use {pdf_to_use} -- recreate {recreate}")
    resultObj.save()
    #return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask",kwargs=dict(taskid = returnValue.id))
    return redirect(url)

def createMissingAbstract_Bodies_in_CollectionFromTEI(request,pk=None):

    pk = request.GET.get("pk",pk)
    try:
        int(pk)
    except:
        pk = None

    if pk is None:
        return render(request,"publicationExtensions/processTEI/chooseCollection.html",
                      {"collections" : Collection.objects.all()})

    collection = get_object_or_404(Collection,pk=pk)
    #mode = request.GET.get("mode",mode)

    returnValue = _extract_collection_tei.delay(collection.pk)
    #returnValue = _extract_collection_tei(collection.pk)
    resultObj = TaskResult(user=request.user,key=returnValue.id,
                           taskname = f"createMissingAbstract_Bodies_in_CollectionFromTEI: {collection.name}")
    resultObj.save()
    #return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask",kwargs=dict(taskid = returnValue.id))
    return redirect(url)


def exportAttachmentsOfCollectionItems(request, pk=None, type_of_text=None, type_of_processing=None): #, format=None):
    pk = request.GET.get("pk", pk)
    try:
        int(pk)
    except:
        pk = None

    type_of_text = request.GET.get("type_of_text", type_of_text)
    type_of_processing = request.GET.get("type_of_processing", type_of_processing)
    #format = request.GET.get("format", format)
    attypes = [a.name for a in AttachmentType.objects.all()]
    prottypes = [a.name for a in ProcessingType.objects.all()]
    #formats = [a.name for a in FormatType.objects.all()]
    #formats = ["pickle","corpus"]
    if pk is None:
        return render(request,"publicationExtensions/exportAttachment/chooseCollection.html",
                      {"collections" : Collection.objects.all(),
                       "types_of_text" : attypes,
                       "types_of_processing": prottypes})
                      # "formats" : formats})


    returnValue = _export_attachments_collection_items.delay(pk, type_of_text, type_of_processing, format)
    collection = Collection.objects.get(pk=pk)
    resultObj = TaskResult(user=request.user, key=returnValue.id,
                           taskname=f"exportAttachmentsOfCollectionItems: {collection.name} - {type_of_text}")
    resultObj.save()
    # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)


def exportCollectionAttachments(request, pk=None, type_of_text=None, type_of_processing=None, format=None):
    pk = request.GET.get("pk", pk)
    try:
        int(pk)
    except:
        pk = None

    type_of_text = request.GET.get("type_of_text", type_of_text)
    type_of_processing = request.GET.get("type_of_processing", type_of_processing)
    format = request.GET.get("format", format)
    attypes = [a.name for a in AttachmentType.objects.all()]
    prottypes = [a.name for a in ProcessingType.objects.all()]
    formats = [a.name for a in FormatType.objects.all()]
    formats = ["pickle","corpus"]
    if pk is None:
        return render(request,"publicationExtensions/exportCollectionAttachment/chooseCollection.html",
                      {"collections" : Collection.objects.all(),
                       "types_of_text" : attypes,
                       "types_of_processing": prottypes,
                        "formats" : formats})


    returnValue = _export_collection_attachments.delay(pk, type_of_text, type_of_processing, format)
    collection = Collection.objects.get(pk=pk)
    resultObj = TaskResult(user=request.user, key=returnValue.id,
                           taskname=f"exportAttachmentsOfCollectionItems: {collection.name} - {type_of_text}")
    resultObj.save()
    # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)



class TaskResultTable(tables.Table):
        #def render_name(self, value, record):
        #    url = reverse("publications:collectionView", kwargs={"pk": record.pk})
         #   return format_html(f"<a href='{url}'>{value}</a>")

        status = tables.Column(empty_values=())

        def render_status(self,value,record):
            task = AsyncResult(record.key)
            return task.status

        def render_key(self,value):

            url = reverse("publicationsExtension:showtask",kwargs=dict(taskid=value))
            return  format_html(f"<a href='{url}'>{value}</a>")

        class Meta:
            model = TaskResult
            template_name = "django_tables2/bootstrap.html"
            #fields = ("name",)
            exclude = ("updated_at",)



class TaskResultFilter(django_filters.FilterSet):

    taskname = django_filters.CharFilter(lookup_expr="contains")

    #date = django_filters.CharFilter(lookup_expr="contains")
    #year = django_filters.NumericRangeFilter()
    #has_pdf =  django_filters.BooleanFilter(label="has_pdf")


    STATUS_CHOICES = (
        ("STARTED", 'STARTED'),
        ("PENDING", 'PENDING'),
        ("FAILURE", 'FAILURE'),
        ("SUCCESS", 'SUCCESS'),
    )

    status = django_filters.ChoiceFilter(choices=STATUS_CHOICES,field_name="key")

    class Meta:
        model = TaskResult
        fields = ["taskname"]

def showAllTasks(request):
    tasks = TaskResult.objects.all()

    res = {x: y for x, y in request.GET.items()}


    tasks_filtered = []
    if res.get("status", "") == "":
        tasks_filtered = tasks
    else:
        for t in tasks:
            task = AsyncResult(t.key)
            if task.status == res.get("status"):
                tasks_filtered.append(t)

    filter = TaskResultFilter(res, queryset=tasks)
    table = TaskResultTable(tasks_filtered)

    table.paginate(page=request.GET.get("page", 1), per_page=25)
    #table.order_by = "title"  # request.GET.get("sort","title")

    return render(request, "task/taskList.html", {
        "table": table,
        "filter" :filter
    })

def index(request):
    return render(request, "publicationExtensions/index.html")

def showTask(request,taskid):
    res = AsyncResult(taskid)

    if res.status == "FAILURE":
        return render(request, "task/task_failure.html", {"status": res.status,
                                                  "result": res.result,
                                                  "task": taskid
                                                  })

    if res.status == "PENDING" or res.status == "STARTED":
        return render(request, "task/task_pending.html", {"status": res.status,
                                                          "result": res.result,
                                                          "task": taskid
                                                          })

    links = []
    result = res.result

    if not isinstance(result,dict): ##old tasks
        result = {"text":result,
                  "related_objects" : []}

    if result and result.get("text",None):
        for l in result["text"]:
            if l.startswith(settings.TEMP_PATH):
                pt = l.replace(settings.TEMP_PATH,"")
                if pt.startswith("/"):
                    pt = pt[1:]
                links.append(pt)

    rel_objects = []
    for r in result["related_objects"]:
        pk = r[0]
        tp = r[1]
        rel_objects.append(globals()[tp].objects.get(pk=pk))


    return render (request, "task/task.html",{"status":res.status,
                                              "result":result.get("text",[]),
                                              "task":taskid,
                                              "relatedObjects":rel_objects,
                                              "links" : links
                                             } )

def processNLP(request,pk=None, mode="lemmatize", type="abstract",
               lang="en", update_only = False):

        pk = request.GET.get("pk", pk)
        try:
            int(pk)
        except:
            pk = None

        type = request.GET.get("type", type)
        mode = request.GET.get("mode", mode)
        lang = request.GET.get("lang", lang)

        _update_only = request.GET.get("update_only", update_only)
        if _update_only == "True":
            update_only = True

        atts = [a.name for a in AttachmentType.objects.all()]
        modes = ['lemmatize','tokenize']
        if pk is None:
            return render(request, "publicationExtensions/processNLP/chooseCollection.html",
                          {"collections": Collection.objects.all(),
                           "types": atts,
                           "modes" : modes})

        col = Collection.objects.get(pk=pk)
        items = col.items.all()

        ## make sure they exist
        _type, _created = AttachmentType.objects.get_or_create(name=type)
        _folder, _created = Folder.objects.get_or_create(name=type)

        for start in range(0,len(items),500):
            returnValue = _NLP_collection_pdf.delay(pk, mode=mode, type_of_text=type, lang=lang, start=start,end=start + 500, update_only=update_only)
            #returnValue = _NLP_collection_pdf(pk, mode=mode, type=type, lang=lang)
            resultObj = TaskResult(user=request.user, key=returnValue.id,
                                   taskname=f"processNLP: {col.name} - {mode} - {type} - {lang} - {start}")
            resultObj.save()

        # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
        url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

        return redirect(url)




def createCorpora(request,pk=None, lang="en", type_of_text="abstract", format = "pickle", store_as_attachment="No"):

    format = request.GET.get("format", format)
    formats = ["pickle", "corpus"]
    if not format in formats:
        return HttpResponse(f"format not in {formats}", status=405)

    pk = request.GET.get("pk", pk)
    try:
        int(pk)
    except:
        pk = None

    type_of_text = request.GET.get("type", type_of_text)
    lang = request.GET.get("lang", lang)
    langs = ["en_vectors_web_lg","en_core_web_lg","de_core_news_lg"]


    _store_as_attachment =   request.GET.get("store_as_attachment", store_as_attachment)
    if _store_as_attachment == "yes":
        store_as_attachment = True
    else:
        store_as_attachment = False

    if pk is None:
        return render(request, "publicationExtensions/createCorpora/chooseCollection.html",
                      {"collections": Collection.objects.all(),
                       "types": ATTACHMENTTYPES,
                       "lang": langs,
                       "formats" : formats})

    col = Collection.objects.get(pk=pk)

    tp = AttachmentType.objects.get_or_create(name = type_of_text)

    returnValue =  _createYearCorpora.delay(pk, lang=lang, type_of_text=type_of_text, format = format,
                                            store_as_attachment = store_as_attachment,
                                            user = request.user.username)
    #returnValue = _createYearCorpora(pk, lang="en", type=type, format=format)
    resultObj = TaskResult(user=request.user, key=returnValue.id,
                           taskname=f"createCorpora: {col.name} - {lang} - {type_of_text} ")
    resultObj.save()
    # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)

def download_from_tmp(self,fn):
    if fn.startswith("/"):
        return HttpResponse(content="fn must not start with '/'!",status=405)
    path = os.path.join(settings.TEMP_PATH,fn)
    if not os.path.exists(path):
        raise Http404(f"path {fn} not found in tmp!")

    fh = open(path,"rb")
    return FileResponse(fh)

def createWordsPerDate(request,collection=None,type_of_text="abstract_years",lang="en",start = None, end=None,
                       format = "pickle",
                       threshold = 50):

    collection = request.GET.get("collection", collection)
    type_of_text = request.GET.get("type_of_text", type_of_text)
    lang = request.GET.get("lang", lang)
    format = request.GET.get("format", format)
    threshold =  request.GET.get("threshold", threshold)
    try:
        start = int(request.GET.get("start", start))
    except TypeError:
        start = None

    try:
        end = int(request.GET.get("end", end))
    except TypeError:
        end = None

    langs = ["en_vectors_web_lg", "en_core_web_lg", "de_core_news_lg"]
    formats = ["pickle","xlsx","csv"]

    if collection is None:
        collections = []
        attypes = set()
        try:
            ptf = ProcessingType.objects.get(name="corpus_year")

            for col in Collection.objects.all():
                for atts in col.collectionattachment_set.filter(type_of_processing=ptf):
                    collections.append(col) # add this because it has attachments
                    try:
                        attypes.add(atts.type_of_text.name)
                    except AttributeError:
                        logger.error(f"{atts} has empty type!")
        except ProcessingType.DoesNotExist:
            return render(request, "publicationExtensions/createWordsPerDate/noCorpus_year.html")

        return render(request, "publicationExtensions/createWordsPerDate/chooseCollection.html",
                      {"collections": Collection.objects.all(),
                       "types": attypes,
                       "lang": langs,
                       "formats": formats})

    threshold = int(threshold)
    returnValue = _createWord_Date.delay(collection, lang, request.user.username, type_of_text,
                                         start, end,format, threshold)

    resultObj = TaskResult(user=request.user, key=returnValue.id,
                           taskname=f"createWordPerDate: {collection} - {lang} - {type_of_text} - {start} - {end}")
    resultObj.save()
    # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)


def createBurst(request,all_r=None, s=None, gam=None):

    all_r = request.GET.get("all_r", all_r)
    s = request.GET.get("s", s)
    gam = request.GET.get("gam", gam)
    _only_years = request.GET.get("only_years", "no")
    only_years = not (_only_years == "no" )
    if not all_r:
        atttype = ProcessingType.objects.all()
        all_r_types = [t for t in atttype if t.name.startswith("all_r") ]

        all_rs = []
        for all_r_type in all_r_types:
            all_rs += CollectionAttachment.objects.filter(type_of_processing=all_r_type)

        if all_r is None:
            return render(request, "publicationExtensions/createBurst/chooseAll_r.html",
                          {"all_rs": all_rs})

    all_r = CollectionAttachment.objects.get(pk=all_r)
    user = request.user.username
    try:
        s = float(s)
        gam = float(gam)
    except:
        all_rs = []
        for all_r_type in all_r_types:
            all_rs += CollectionAttachment.objects.filter(type_of_processing=all_r_type)

        return render(request, "publicationExtensions/createBurst/chooseAll_r.html",
               {"all_rs": all_rs})

    returnValue = _createBurst.delay(all_r.pk, user, s, gam, only_years)

    resultObj = TaskResult(user=request.user, key=returnValue.id,
                           taskname=f"createBurst: {all_r} - {s} - {gam}")
    resultObj.save()
    # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
    url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)

def plot_burst(request,burst_pk,threshold = None):

    burst = get_object_or_404(CollectionAttachment, pk = burst_pk)
    pt = burst.type_of_processing.name
    #"burst_{s}_{gam}_yea
    splitted = pt.split("_")
    if splitted[-1] == "month":
        typ ="year_month"
    else:
        typ = "year"

    s = splitted[1]
    gam = splitted[2]

    all_bursts = pandas.read_excel(burst.file.file.path)

    attms = burst.derived_from_collection_attachment.filter()
    if len(attms) == 0:
        raise Http404("No all_r attachment")

    for attm in attms:
        pt = attm.type_of_processing.name
        if threshold:
            tp = "all_r_{threshold}"
            if pt == tp:
                all_r_attachment = attm

        else:
            if pt.startswith("all_r"):
                all_r_attachment = attm

    all_r = pandas.read_pickle(all_r_attachment.file.file.path)

    attms = all_r_attachment.derived_from_collection_attachment.filter()
    if len(attms) == 0:
        raise Http404("No word_count attachment")
    for attm in attms:
        pt = attm.type_of_processing.name
        if pt == "wordcounts":
            word_count_attm = attm

    word_list = pandas.read_csv(word_count_attm.file.file.path)
    collection = burst.parent
    d = pandas.Series(collection.timeDistribution())
    d = d.to_frame()
    #d = d.rename({"level_0": "_month", "level_1" : "_year", 2: "value"}, axis=1)
    d.index = d.index.set_names(['_month', '_year'])
    #d = d.set_index(["_year", "_month"])
    d = d.swaplevel(0,1)
    d = d.sort_index()
    plotter = BurstPlotter(typ, all_bursts, word_list, all_r, d, s, gam,start_year = 0, end_year = 9999)

    n_bursts = 100
    cnt = 0
    top_noun = []

    ## filter short words
    filter_words = """fig
    www
    org
    edp
    http:/
    https:/
    chapter
    user
    sciences
    ‫ע‬
    ðnd""".split("\n")

    for b in all_bursts.sort_values(by='weight', ascending=False).reset_index(drop=True).iterrows():
        try:
            if ("@NOUN" in b[1]["label"]) or ("@PROPN" in b[1]["label"]):
                a, end = b[1]["label"].split("@")
                if a in filter_words:
                    continue
                print(a, len(a))
                if len(a) < 3:
                    continue
                if "-" in a:
                    continue
                top_noun.append(b[1])
                cnt += 1
                if cnt >= n_bursts:
                    break
        except:
            pass

    top_bursts = pandas.DataFrame(top_noun)
    # sort bursts by end date
    sorted_bursts = top_bursts.sort_values('end', ascending=False).reset_index(drop=True)

    a = sorted_bursts[sorted_bursts.loc[:, 'end'] == sorted_bursts.loc[:, 'begin']]
    a["end"] += 0.5
    smaller = sorted_bursts.drop(a.index)
    top_bursts = smaller.append(a)
    dist_ticks = 10

    x_ticks_label = [d.index[x] for x in range(0, len(d), dist_ticks)]

    cmap = mpl.cm.cool
    norm = mpl.colors.Normalize(vmin=min(top_bursts["weight"]), vmax=max(top_bursts["weight"]))

    fig, ax = plt.subplots(figsize=(6, 1))
    fig.subplots_adjust(bottom=0.5)

    fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
                 cax=ax, orientation='horizontal', label='weights')

    colorbar_image = currentPltToImage()


    sorted_bursts = top_bursts.sort_values('end', ascending=False).reset_index(drop=True)
    # for bursts that end at the last timepoint, sort by start point
    last_timepoint = np.max(sorted_bursts['end'])
    sorted_bursts.loc[sorted_bursts['end'] == last_timepoint, :] = sorted_bursts.loc[
                                                                   sorted_bursts['end'] == last_timepoint, :] \
        .sort_values(by='begin', ascending=False) \
        .reset_index(drop=True)

    # format bars
    bar_width = 0.8  # width of bars
    bar_pos = np.array(range(len(sorted_bursts)))  # positions of top edge of bars
    ylabel_pos = bar_pos + (bar_width / 2)  # y axis label positions
    n = len(all_r)  # number of time points

    # initialize the matplotlib figure
    f, ax = plt.subplots(figsize=(20, 25))

    # plot current bursts in blue and old bursts in gray
    sorted_bursts['color'] = [cmap(norm(x)) for x in sorted_bursts["weight"]]
    # sorted_bursts.loc[sorted_bursts['end']==last_timepoint,'color'] = blog_blue

    # plot the end points
    end_bars = ax.barh(bar_pos, sorted_bursts.loc[:, 'end'], bar_width, align='edge',
                       color=sorted_bursts['color'], edgecolor='none')

    # plot the start points (in white to blend in with the background)
    start_bars = ax.barh(bar_pos, sorted_bursts.loc[:, 'begin'], bar_width, align='edge',
                         color='w', edgecolor='none')

    # label each burst
    plt.yticks(ylabel_pos, '')  # remove default labels
    for bs in range(len(sorted_bursts)):
        try:
            width = int(end_bars[bs].get_width())
        except:
            width = 0
        # place label on right side for early bursts
        if width <= (n / 2):
            # if False:
            plt.text(width + 4, max(ylabel_pos[bs], 0), sorted_bursts.loc[bs, 'label'],
                     fontsize=12, va='center')
        # place label on left side for late bursts
        else:
            try:
                width = int(start_bars[bs].get_width())
            except:
                width = 0
            plt.text(width - 4, ylabel_pos[bs], sorted_bursts.loc[bs, 'label'],
                     fontsize=12, va='center', ha='right')

    # format plot
    ax.set(xlim=(0, n), ylim=(0, n_bursts + 1), ylabel='', xlabel='')
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_color([0.9, 0.9, 0.9])
    ax.spines["right"].set_color([0.9, 0.9, 0.9])
    for x in range(0, n):
        if (x + 1) % 24 == 0:
            plt.axvline(x, color='gray', linestyle='-', linewidth=0.5, alpha=0.25)
    plt.xticks(range(0, n, dist_ticks), x_ticks_label, rotation='vertical')

    ax.set_title(f'Timeline of the top {n_bursts} bursting" topics in the  exo literature (s: {s}, gam: {gam})',
                 size=14)

    #plt.savefig(f"bursts_top_{n_bursts}_s{s}_g{gam}_n{n}.png", bbox_inches="tight", dpi=300)

    # Store image in a string buffer

    burst_image = currentPltToImage()

    return render(request,"publicationExtensions/burstPlotter/plot_burst.html",{"plot_d" : plotter.plot_d(),
                                                                             "colorbar_image" : colorbar_image,
                                                                            "burst_image" : burst_image,
                                                                                "burst": burst,
                                                                                "collection" : collection})


def plot_d(request,collection_pk):

    collection = get_object_or_404(Collection,pk = collection_pk)
    d = pandas.Series(collection.timeDistribution())
    d.index = d.index.swaplevel(0,1)
    d = d.sort_index()
    x = d.iplot(asFigure=True)
    div = iplot(x, output_type='div', include_plotlyjs=True)

    return render(request,"publicationExtensions/burstPlotter/plot_d.html",{"plot_d" : div})

def pdf_unite(request,publication_pk):
    ## Führe pdf pages zu einem PDF zusammen

    publication = get_object_or_404(Publication,pk=publication_pk)
    p_s_rs = publication.publicationsourcereference_set.all()
    for p_s_r in p_s_rs:
        #returnValue = _pdfUnite(p_s_r)
        returnValue = _pdfUnite.delay(p_s_r.pk)
        resultObj = TaskResult(user=request.user, key=returnValue.id,
                               taskname=f"pdfunite: {publication_pk}")
        resultObj.save()
        # return render(request,"publicationExtensions/pdftotext/result.html",{ "returnValue": [returnValue]})
        url = reverse("publicationsExtension:showtask", kwargs=dict(taskid=returnValue.id))

    return redirect(url)
