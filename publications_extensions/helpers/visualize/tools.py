import io
import matplotlib as mpl
import PIL
import base64

def currentPltToImage():
    buffer = io.BytesIO()
    canvas = mpl.pyplot.get_current_fig_manager().canvas
    canvas.draw()
    pilImage = PIL.Image.frombytes("RGB", canvas.get_width_height(), canvas.tostring_rgb())
    pilImage.save(buffer, "PNG")
    mpl.pyplot.close()
    # color_bar_div = iplot(tools.mpl_to_plotly(plt), auto_open=False, output_type="div")
    buffer.seek(0)
    img_data = base64.b64encode(buffer.read())
    image = f"data:image/png;base64,{img_data.decode('ascii')}"
    return  image