from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from documents.models import Source
from publications.models import Publication


class TaskResult(models.Model):

    user = models.ForeignKey(User,on_delete=models.SET_NULL, null=True)
    key = models.CharField(max_length=1000)
    taskname =  models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return "%s_%s" % (self.user,self.key)

class PublicationSourceReference(models.Model):
    """Class to connect the two apps"""
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.source.pk}_{self.publication.pk}"


