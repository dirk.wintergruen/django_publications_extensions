import os,os.path
import sys

from django.core.management import BaseCommand
from django.core.files import File

from publications.models import Publication
from publications.models.attachment import URLAttachment, PDFAttachment, Attachment, AttachmentType
from publications.models.tag import Tag
import logging

from publications_extensions.helpers import filesHandler

logger = logging.getLogger(__name__)
from filer.models import File as Filer_File, Folder


class Command(BaseCommand):
    help = """adds files as attachment to zotero. Here the filenames are zotero_keys of publications.                                                                                                                       
           Links will be added as URLAttachment to the Object with the key."""


    def add_arguments(self, parser):
        parser.add_argument('--in_folder', type=str, required = True)
        parser.add_argument('--out_folder', type=str, required=True, help="Folder name in django filer")
        parser.add_argument("--tags", type=str,default="", help="comma separated list of tags")
        parser.add_argument("--type", type=str, required= True, help="type of the attachment")
        parser.add_argument("--always_upload", default=False, const=True, nargs="?",
                            help="always upload the file, normally checks if file with same sha already exists.")

    def handle(self, *args, **options):
        type = options["type"]

        try:
            type = AttachmentType.objects.get(name=type)
        except AttachmentType.DoesNotExist:
            print(f"Type {type} is not a registered type please create it first in django!")
            return

        in_folder = options["in_folder"]
        always_upload = options["always_upload"]
        folder_name =  options["out_folder"]

        if not os.path.exists(in_folder):
            print(f"{in_folder} does not exist")
            sys.exit(-1)

        tags = []
        for tag in options["tags"].split(","):
            new_tag,created = Tag.objects.get_or_create(name=tag)
            tags.append(new_tag)
            new_tag.save()

        for f in os.listdir(in_folder):

            url = "file://" + os.path.join(in_folder,f)
            #parent_key,ext = os.path.splitext(f)
            splitted = f.split(".")
            parent_key = splitted[0]

            #two possibilities --- filename is key of a publication object or of an PDF attachement try first publications:
            publications = Publication.objects.filter(zoterokey=parent_key)
            if len(publications) == 0:
                publications = Publication.objects.filter(zoterokey=parent_key.lower())
            if len(publications) == 0:
                publications = Publication.objects.filter(zoterokey=parent_key.upper())

            if len(publications) == 0:
                print(f"Parent with id {parent_key} doesn't exist!, will try PDFAttachment")
                pdfs = PDFAttachment.objects.filter(zoterokey=parent_key)
                if len(pdfs) == 0:
                    pdfs = PDFAttachment.objects.filter(zoterokey=parent_key.lower())
                if len(pdfs) == 0:
                    pds = PDFAttachment.objects.filter(zoterokey=parent_key.upper())
                    if len(pdfs) == 0:
                        logging.info(f"Cannot identify publication {parent_key} where the object belongs to. Ignore it.")
                        continue
                publications = []
                for pdf in pdfs:
                    publications.append(pdf.parent)

            if len(publications) == 0:
                logging.info(f"Cannot identify publication {parent_key} where the object belongs to but found a pdf attachement. Ignore it.")
                continue

            for publication in publications:

                # create an urlattachment
                objs_tmp = URLAttachment.objects.filter(url=url)
                objs = [o for o in objs_tmp if o.parent == publication]
                if len(objs) == 0:
                    urlattachment = URLAttachment(parent=publication,url=url)
                elif len(objs)> 1:
                    raise ValueError
                else:
                    urlattachment = objs[0]
                urlattachment.save()
                for t in tags:
                    urlattachment.tags.add(t)
                urlattachment.type = type
                urlattachment.save()

                # create an pdfattachment
                path= os.path.join(in_folder,f)
                new_obj = filesHandler.uploadOrGetExistingFile(path,
                                                               f,
                                                               folder_name,
                                                               options["always_upload"])

                logger.debug(f"will create attachment now")

                attachment, created = Attachment.objects.get_or_create(parent=publication,name=f,type=type)
                if created:
                    logger.debug("created new pdf attachment!")
                attachment.file = new_obj
                logger.debug(f"adding tags")
                for t in tags:
                    attachment.tags.add(t)
                # attachment.parent = Publication.objects.get_or_create(zoterokey=parentItem)
                new_obj.save()
                attachment.save()
                attachment.related_URLAttachment.add(urlattachment)
                #attachment.type = type
                attachment.save()