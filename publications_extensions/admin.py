from django.contrib import admin

# Register your models here.
from publications_extensions.models import TaskResult

admin.site.register(TaskResult)